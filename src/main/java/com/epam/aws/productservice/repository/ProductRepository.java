package com.epam.aws.productservice.repository;

import com.amazonaws.xray.spring.aop.XRayEnabled;
import com.epam.aws.productservice.model.Product;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

@XRayEnabled
@EnableScan
public interface ProductRepository extends CrudRepository<Product, Long> {
}
