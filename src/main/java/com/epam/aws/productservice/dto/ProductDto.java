package com.epam.aws.productservice.dto;

import lombok.Data;

@Data
public class ProductDto {

    private String name;
}
