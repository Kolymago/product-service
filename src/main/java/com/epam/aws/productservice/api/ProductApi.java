package com.epam.aws.productservice.api;

import com.epam.aws.productservice.dto.ProductDto;
import com.epam.aws.productservice.model.Product;
import com.epam.aws.productservice.repository.ProductRepository;
import com.epam.aws.productservice.service.ProductService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/product")
@RequiredArgsConstructor
public class ProductApi {

    private final ProductRepository productRepository;
    private final ProductService productService;

    @PostMapping("/{id}")
    public HttpEntity<Product> create(@PathVariable final Long id, @RequestBody final ProductDto productDto) {
        return ResponseEntity.ok(productRepository.save(new Product(id, productDto.getName())));
    }

    @GetMapping("/{id}")
    public HttpEntity<Product> get(@PathVariable final Long id) {
        return Optional.ofNullable(productService.getById(id))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/version")
    public HttpEntity<String> getVersion() {
        return ResponseEntity.ok("1");
    }

}
