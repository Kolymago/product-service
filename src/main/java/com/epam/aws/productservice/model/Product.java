package com.epam.aws.productservice.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@DynamoDBTable(tableName = "Product")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    @DynamoDBHashKey(attributeName = "Id")
    private Long id;

    @DynamoDBAttribute(attributeName = "Name")
    private String name;
}
