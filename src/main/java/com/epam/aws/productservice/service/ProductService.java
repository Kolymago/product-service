package com.epam.aws.productservice.service;

import com.epam.aws.productservice.model.Product;
import com.epam.aws.productservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@CacheConfig(cacheNames = "mu-cache-001")
public class ProductService {

  private final ProductRepository productRepository;

  @Cacheable(value = "getById", key = "{#id}")
  public Product getById(final Long id) {
    return productRepository.findById(id)
        .orElseThrow(() -> new IllegalArgumentException("Product with id: " + id + " not found."));
  }

  @CacheEvict
  public void deleteProduct(final Long id) {
    productRepository.deleteById(id);
  }

  @CachePut(key = "#id")
  public Product updateProduct(final Long id, final Product productInput) {
    Product product = new Product();
    product.setId(id);
    product.setName(productInput.getName());
    return productRepository.save(product);
  }

}
