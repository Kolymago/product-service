ARG BUILD_IMAGE=maven:3.6.3-jdk-11
ARG RUNTIME_IMAGE=openjdk:11.0.7-jre-slim

FROM ${BUILD_IMAGE} as dependencies

WORKDIR /build
COPY Dockerfile /build
COPY pom.xml /build/

FROM dependencies as build

WORKDIR /build
COPY src /build/src/

RUN mvn -B clean package

FROM ${RUNTIME_IMAGE}

WORKDIR /app
COPY --from=build /build/target/*.jar /app/product-service.jar

CMD ["sh", "-c", "java -XX:+UseContainerSupport -jar /app/product-service.jar"]